package auth

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.configuration.RunConfiguration

import internal.GlobalVariable

public class Authenticate {
	@Keyword
	public static void performLogin() {
		WebUI.openBrowser('')

		WebUI.deleteAllCookies()

		WebUI.maximizeWindow()

		WebUI.navigateToUrl('https://demo-app.online/')

		WebUI.click(findTestObject('Object Repository/or1_Website/orw2_login/Page_Be a Profressional Talent with Coding.ID/a_Masuk'))

		WebUI.setText(findTestObject('Object Repository/or1_Website/orw2_login/Page_Masuk untuk dapatkan akses di Coding.ID/input_Email_email'),
				'adinatarezawahyu@gmail.com')

		WebUI.setEncryptedText(findTestObject('Object Repository/or1_Website/orw2_login/Page_Masuk untuk dapatkan akses di Coding.ID/input_Kata                                 _98da12'),
				'Gz08DaJU9NXyFmL46LbsMw==')

		WebUI.click(findTestObject('Object Repository/or1_Website/orw2_login/Page_Masuk untuk dapatkan akses di Coding.ID/button_Login'))

		WebUI.verifyElementText(findTestObject('Object Repository/or1_Website/orw2_login/Page_Be a Profressional Talent with Coding.ID/h5_Bootcamp untuk semua,            dengan _5cbf2e'),
				'Bootcamp untuk semua, dengan background IT maupun Non-IT. Pilih programnya dan dapatkan jaminan kerja.', FailureHandling.CONTINUE_ON_FAILURE)
	}
	@Keyword
	public static void performLoginProfile() {
		WebUI.openBrowser('')

		WebUI.deleteAllCookies()

		WebUI.maximizeWindow()

		WebUI.navigateToUrl('https://demo-app.online/')

		WebUI.click(findTestObject('Object Repository/or1_Website/orw2_login/Page_Be a Profressional Talent with Coding.ID/a_Masuk'))

		WebUI.setText(findTestObject('Object Repository/or1_Website/orw2_login/Page_Masuk untuk dapatkan akses di Coding.ID/input_Email_email'),
				'adinatarezawahyu@gmail.com')

		WebUI.setEncryptedText(findTestObject('Object Repository/or1_Website/orw2_login/Page_Masuk untuk dapatkan akses di Coding.ID/input_Kata                                 _98da12'),
				'Gz08DaJU9NXyFmL46LbsMw==')

		WebUI.click(findTestObject('Object Repository/or1_Website/orw2_login/Page_Masuk untuk dapatkan akses di Coding.ID/button_Login'))

		WebUI.verifyElementText(findTestObject('Object Repository/or1_Website/orw2_login/Page_Be a Profressional Talent with Coding.ID/h5_Bootcamp untuk semua,            dengan _5cbf2e'),
				'Bootcamp untuk semua, dengan background IT maupun Non-IT. Pilih programnya dan dapatkan jaminan kerja.', FailureHandling.CONTINUE_ON_FAILURE)

		WebUI.click(findTestObject('Object Repository/or1_Website/orw3_updateProfile/Page_Be a Profressional Talent with Coding.ID/a'))

		WebUI.click(findTestObject('Object Repository/or1_Website/orw3_updateProfile/Page_Be a Profressional Talent with Coding.ID/a_My Account'))

		WebUI.click(findTestObject('Object Repository/or1_Website/orw3_updateProfile/Page_Coding.ID - Dashboard/span_Profil'))
	}
	@Keyword
	public static void performLoginProfileMobile() {
		String absoluteFilePath = RunConfiguration.getProjectDir() + "\\OurFile\\DemoAppV2.apk"

		Mobile.startApplication(absoluteFilePath, false)

		//		Mobile.startExistingApplication('com.codingid.codingidhive.betastaging')


		Mobile.tap(findTestObject('Object Repository/or2_Mobile/orm3_updateProfile/Home/Button - Login Here'), 0)

		Mobile.tap(findTestObject('Object Repository/or2_Mobile/orm3_updateProfile/Login/Email/Input - Email'), 0)

		Mobile.setText(findTestObject('Object Repository/or2_Mobile/orm3_updateProfile/Login/Email/Input - Email'), 'adinatarezawahyu@gmail.com', 0)

		Mobile.tap(findTestObject('Object Repository/or2_Mobile/orm3_updateProfile/Login/Email/Input - Email'), 0)

		Mobile.setEncryptedText(findTestObject('Object Repository/or2_Mobile/orm3_updateProfile/Login/Password/Input - Password'), 'Gz08DaJU9NXyFmL46LbsMw==', 0)

		Mobile.tap(findTestObject('Object Repository/or2_Mobile/orm3_updateProfile/Login/Button - Login'), 0)
	}
	@Keyword
	public static void performLogOutProfileMobile() {

		Mobile.tap(findTestObject('Object Repository/or2_Mobile/orm3_updateProfile/Home/Button - Profile'), 0)

		Mobile.tap(findTestObject('Object Repository/or2_Mobile/orm3_updateProfile/Home/Button - Settings'), 0)

		Mobile.tap(findTestObject('Object Repository/or2_Mobile/orm3_updateProfile/Home/Button - Logout'), 0)
	}
}
