import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

CustomKeywords.'auth.Authenticate.performLoginProfile'()

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/or1_Website/orw3_updateProfile/Page_Coding.ID - Dashboard/a_Change Password'))

WebUI.navigateToUrl('https://demo-app.online/dashboard/profile/change_password')

WebUI.setEncryptedText(findTestObject('Object Repository/or1_Website/orw3_updateProfile/Page_Coding.ID - Dashboard/input_Old Password_current_password'), 
    'Gz08DaJU9NXbAB8Pb2/rbg==')

WebUI.setEncryptedText(findTestObject('Object Repository/or1_Website/orw3_updateProfile/Page_Coding.ID - Dashboard/input_New Password_password'), 
    'Gz08DaJU9NXyFmL46LbsMw==')

WebUI.setEncryptedText(findTestObject('Object Repository/or1_Website/orw3_updateProfile/Page_Coding.ID - Dashboard/input_Confirmation Password_password_confirmation'), 
    'Gz08DaJU9NXyFmL46LbsMw==')

WebUI.click(findTestObject('Object Repository/or1_Website/orw3_updateProfile/Page_Coding.ID - Dashboard/button_Save Changes'))

WebUI.verifyElementVisible(findTestObject('Object Repository/or1_Website/orw3_updateProfile/Page_Coding.ID - Dashboard/small_The password is incorrect'))

WebUI.verifyElementText(findTestObject('Object Repository/or1_Website/orw3_updateProfile/Page_Coding.ID - Dashboard/small_The password is incorrect'), 
    'The password is incorrect.')

WebUI.closeBrowser()

