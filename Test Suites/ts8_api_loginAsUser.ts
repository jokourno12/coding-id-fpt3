<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>ts8_api_loginAsUser</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>b71ab669-ec56-41f6-88a1-67bf8dcc56a3</testSuiteGuid>
   <testCaseLink>
      <guid>772ef0a6-5fe2-4e7b-8364-f8a2ed15e55e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/tc3_API/p1_login/al1_email kosong</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>ca0532b1-6c7e-414e-858c-e51238b766d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/tc3_API/p1_login/al2_password kosong</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>704b06dc-09d6-44ef-a57d-58bf1ded243c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/tc3_API/p1_login/al3_email dan password kosong</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>e5ce77ad-7dea-49de-a375-29253e810cf1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/tc3_API/p1_login/al4_email salah dan password valid</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>fe25f072-f524-43af-b593-ab73be25759c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/tc3_API/p1_login/al5_password salah dan email valid</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>5c2b7eb5-a693-446b-a46f-3eeec254909b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/tc3_API/p1_login/al6_email dan password tidak valid</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>9bc73444-030a-40a8-9051-064118fad799</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/tc3_API/p1_login/al7_email dan password valid</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>fe3b6e5b-f94f-4e8c-81ef-dcbaa924bd1f</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Data Api Login</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>fe3b6e5b-f94f-4e8c-81ef-dcbaa924bd1f</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>email</value>
         <variableId>1a8631b4-e47f-4902-a819-083724b133d7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>fe3b6e5b-f94f-4e8c-81ef-dcbaa924bd1f</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>c94374b8-5d20-4c54-9dca-f3631b4d450b</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>84abc898-b360-42bb-9d39-f550b382a02b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/tc3_API/p1_login/al8_melakukan authentikasi berulang</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
