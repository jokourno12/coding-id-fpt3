<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>ts4_web_flowBeliEvent</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>2</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>8005c920-2ad1-4004-a97e-7e178ad84741</testSuiteGuid>
   <testCaseLink>
      <guid>ccb2a958-353f-4dd3-9f4f-240b06d28de2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/tc1_Website/w4_flowBeliEvent/wm11_Pengecekan riwayat pembelian dalam menu checkout</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>28c8a6a0-cc81-4034-b531-75d1236f32f3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/tc1_Website/w4_flowBeliEvent/wm12_Pengecekan penghapusan events yang akan di beli</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>1bb3bb14-75fb-450a-8d59-e39b2d1df256</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/tc1_Website/w4_flowBeliEvent/wm10_Melakukan checkout pembelian</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>a41d3621-4acc-4ca8-9676-fa6517ecaacd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/tc1_Website/w4_flowBeliEvent/wm1_Pengecekan pada tampilan menu event yang tersedia</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>931e33ff-be4b-4532-bb36-9f5a64ef16ec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/tc1_Website/w4_flowBeliEvent/wm2_Melakukan pencarian events yang ingin diikuti</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>77ba842b-1406-4825-992e-1ff896a07bf3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/tc1_Website/w4_flowBeliEvent/wm3_Pada tampilan pembelian event mereload tampilan untuk mengecek waktu pendaftaran tersisa</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>1be6faa4-59e2-4c9a-ac20-d7f4f9ef57ee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/tc1_Website/w4_flowBeliEvent/wm4_Pengecekan waktu pendaftaran tersisa ketika keterangan yang diberikan Close</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>d3a49a9d-65f5-4835-b9ba-3975552f5f95</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/tc1_Website/w4_flowBeliEvent/wm5_Melakukan pembelian event</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>f7a572c0-1bf4-47b8-b629-1e7a922517a2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/tc1_Website/w4_flowBeliEvent/wm6_Melakukan pengecek tombol pada lihat Event lainnya pada notifikasi setelah membeli</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>32ded074-f161-45d5-bb61-4aaee22d2dde</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/tc1_Website/w4_flowBeliEvent/wm7_Tampilan pada menu Checkout memilih event yang ingin di beli</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>04a6a390-46fe-4316-ad0d-559874cf1e97</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/tc1_Website/w4_flowBeliEvent/wm8_Melakukan pembelian event pada menu checkout sesuai harga dan total yang ingin di beli</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>47ff7f35-9336-42e6-bd71-67c8169483bd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/tc1_Website/w4_flowBeliEvent/wm9_Melakukan pembelian event berulang</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
