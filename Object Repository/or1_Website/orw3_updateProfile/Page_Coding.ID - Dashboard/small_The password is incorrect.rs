<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>small_The password is incorrect</name>
   <tag></tag>
   <elementGuidId>651589da-4716-44bf-8104-3d3582860f5f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>small</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div[2]/form/div/span/small</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>small</value>
      <webElementGuid>3f95b092-8a4a-49bf-b163-9b47a0abfe87</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>The password is incorrect.</value>
      <webElementGuid>8d390446-f096-4c0f-9514-6ca50295759a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/div[@class=&quot;main-content&quot;]/section[@class=&quot;section&quot;]/div[@class=&quot;section-body&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/form[1]/div[@class=&quot;form-group&quot;]/span[@class=&quot;invalid-feedback&quot;]/small[1]</value>
      <webElementGuid>897be8e7-ee83-4a2d-9a86-9df5d9274488</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div[2]/form/div/span/small</value>
      <webElementGuid>98d59581-4574-4b52-9d2e-82136ac4eec5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Old Password'])[1]/following::small[1]</value>
      <webElementGuid>021bb6de-1446-4f15-a9e9-8707576bd169</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Change Password'])[1]/following::small[1]</value>
      <webElementGuid>75ae048d-4ec7-412d-86ce-8e013f7980df</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New Password'])[1]/preceding::small[1]</value>
      <webElementGuid>428b8216-d325-4b93-9543-94590da9a70d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Confirmation Password'])[1]/preceding::small[1]</value>
      <webElementGuid>cdafa8f8-c047-42f3-90ad-1c009a1a3158</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='The password is incorrect.']/parent::*</value>
      <webElementGuid>ecd682d8-990d-4e97-9e8d-0edeac94ddf9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//small</value>
      <webElementGuid>f95d49ab-8006-4eb9-8617-456c31e249a6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//small[(text() = 'The password is incorrect.' or . = 'The password is incorrect.')]</value>
      <webElementGuid>79c62a25-bd33-4542-90a6-75f515009db9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
