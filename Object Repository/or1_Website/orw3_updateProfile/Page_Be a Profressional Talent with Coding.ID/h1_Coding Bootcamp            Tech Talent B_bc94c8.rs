<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h1_Coding Bootcamp            Tech Talent B_bc94c8</name>
   <tag></tag>
   <elementGuidId>15e2a204-c3e1-4ab5-94f6-1407846ed3a6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-body-homepage']/div/h1</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h1.new_headline_h3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>ca17bd4e-6f99-4294-b09c-361c7c081a95</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>new_headline_h3</value>
      <webElementGuid>7b54a0b6-fb65-4b59-beb0-43d01ed26beb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Coding Bootcamp
            Tech Talent Berkualitas</value>
      <webElementGuid>11ba0128-5bda-400b-9514-da58f0940bc8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-body-homepage&quot;)/div[@class=&quot;containerHeader&quot;]/h1[@class=&quot;new_headline_h3&quot;]</value>
      <webElementGuid>fc3d1825-102d-4ee7-8a4a-5d398eefd065</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-body-homepage']/div/h1</value>
      <webElementGuid>84ce0d4e-5ea6-4967-8eef-8ddf89d61378</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quality Assurance Engineer Class'])[2]/preceding::h1[1]</value>
      <webElementGuid>f62ad427-2b44-421a-b35e-e0296560ac6c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.'])[1]/preceding::h1[1]</value>
      <webElementGuid>b0c4d4ee-85c7-415a-9382-45c11355ab95</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Coding Bootcamp']/parent::*</value>
      <webElementGuid>8ccc6662-a50b-4df1-8498-a40af9fdb073</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h1</value>
      <webElementGuid>a7c596f1-1c28-4b2a-800b-13a2e8ccea1d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = 'Coding Bootcamp
            Tech Talent Berkualitas' or . = 'Coding Bootcamp
            Tech Talent Berkualitas')]</value>
      <webElementGuid>26e3c04b-1d6e-4e3f-a595-432c650ce7aa</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
