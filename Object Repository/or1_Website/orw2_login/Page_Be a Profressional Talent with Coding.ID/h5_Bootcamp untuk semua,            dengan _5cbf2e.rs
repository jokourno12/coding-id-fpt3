<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h5_Bootcamp untuk semua,            dengan _5cbf2e</name>
   <tag></tag>
   <elementGuidId>45c8c347-87ee-493a-9be8-58e5d0897998</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-body-homepage']/div/h5</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h5.new_body1_regular</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h5</value>
      <webElementGuid>0fe87e66-194c-45c7-8b82-8000c2f893f8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>new_body1_regular</value>
      <webElementGuid>71256668-a24a-4abe-b1f9-2e85d132840d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Bootcamp untuk semua,
            dengan
            background IT maupun Non-IT.
            Pilih programnya dan dapatkan jaminan kerja.</value>
      <webElementGuid>f3f3d1e2-3665-43ae-92ff-cc7effbd3977</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-body-homepage&quot;)/div[@class=&quot;containerHeader&quot;]/h5[@class=&quot;new_body1_regular&quot;]</value>
      <webElementGuid>5f4e5a15-89b7-447d-9d0f-b84b4035e50e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-body-homepage']/div/h5</value>
      <webElementGuid>b3604710-2aca-403d-82d9-09f0f80151fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quality Assurance Engineer Class'])[2]/preceding::h5[1]</value>
      <webElementGuid>8a7b485d-a936-4c7f-8308-e6c84b372465</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.'])[1]/preceding::h5[2]</value>
      <webElementGuid>a2839783-1a20-4a5f-b6fd-7238eeac3500</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/h5</value>
      <webElementGuid>9af0fa42-b64c-4979-8567-caad77365eff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h5[(text() = 'Bootcamp untuk semua,
            dengan
            background IT maupun Non-IT.
            Pilih programnya dan dapatkan jaminan kerja.' or . = 'Bootcamp untuk semua,
            dengan
            background IT maupun Non-IT.
            Pilih programnya dan dapatkan jaminan kerja.')]</value>
      <webElementGuid>51ae283d-1056-481d-a382-2ca1c57b655a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
