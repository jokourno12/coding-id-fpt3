<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_until                                 _747c2b</name>
   <tag></tag>
   <elementGuidId>acaa5649-4866-4a9b-ac7f-3ba3273af935</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//li[@id='blockListEvent']/a/div/div[2]/h5/span)[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>782ba4f3-da30-4d5d-854e-18c83c4fc39e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>until
                                                        25 Nov 2023 11:30 WIB</value>
      <webElementGuid>d194dcf5-93ca-4b5e-9594-3f813c33981b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;loopevent&quot;)/li[@id=&quot;blockListEvent&quot;]/a[1]/div[@class=&quot;cardOuter&quot;]/div[@class=&quot;blockBody&quot;]/h5[@class=&quot;textOpen&quot;]/span[1]</value>
      <webElementGuid>a0d95e95-23e9-40ce-b710-1da07e76359f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>(//li[@id='blockListEvent']/a/div/div[2]/h5/span)[2]</value>
      <webElementGuid>0d2f024e-7295-4e60-ac82-f2812ef12b59</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='With Ziyad Syauqi Fawwazi'])[2]/following::span[1]</value>
      <webElementGuid>8cbcef47-890d-4b0f-a969-29b5f4159ce9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mini Class'])[2]/preceding::span[1]</value>
      <webElementGuid>87c2216f-e451-4fa2-93c0-be1fdb9196a7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Day 4: Workshop'])[2]/preceding::span[1]</value>
      <webElementGuid>1045c286-68d4-47b8-b853-17657ca84faa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/a/div/div[2]/h5/span</value>
      <webElementGuid>3a221152-5a44-4ae1-898a-2aeecf5e91da</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'until
                                                        25 Nov 2023 11:30 WIB' or . = 'until
                                                        25 Nov 2023 11:30 WIB')]</value>
      <webElementGuid>39bd6e9a-3679-4dd7-aea7-f02f9ff44738</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
