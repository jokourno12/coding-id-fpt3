<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Events                                 _d6b973</name>
   <tag></tag>
   <elementGuidId>c3ca56d4-440c-40e3-b97b-1d02ec8be591</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#containerEvent > div</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='containerEvent']/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>b815b114-3df5-4114-bf34-7c29f16049a3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>wire:id</name>
      <type>Main</type>
      <value>LUXUpzYC1gjiyzfIbbjW</value>
      <webElementGuid>cac42664-7648-4608-a1ab-73359de99730</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
        
            Events
        
        

            
        
    


    
                    
                
                    
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 3: Predict using Machine Learning
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            OPEN until
                                                        18 Nov 2023 11:30 WIB
                                                
                                                                                        Mini Class
                                            
                                            
                                                Day 3: Predict using Machine Learning
                                                
                                            
                                            
                                                18 Nov 2023 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    Rp
                                                                85.000
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 4: Workshop
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            OPEN until
                                                        25 Nov 2023 11:30 WIB
                                                
                                                                                        Mini Class
                                            
                                            
                                                Day 4: Workshop
                                                
                                            
                                            
                                                25 Nov 2023 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    Rp
                                                                85.000
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 2: Data Wrangling with Python
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                Day 2: Data Wrangling with Python
                                                
                                            
                                            
                                                11 Nov 2022 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    Rp
                                                                85.000
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 1: Introduction to Python for Data Scientist
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                Day 1: Introduction to Python for Data Scientist
                                                
                                            
                                            
                                                04 Nov 2022 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    Rp
                                                                85.000
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Booster Class: Be Data Scientist and Machine Learning Engineer in 4 Days
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                Booster Class: Be Data Scientist and Machine Learning Engineer in 4 Days
                                                
                                            
                                            
                                                04 Nov 2022 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            2.000.000

                                                                                                                    Rp
                                                                200.000
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Automation Testing 101 (Demo Using Katalon)
                                                    
                                                
                                                
                                                    With Impola Tonatua Surya
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                Automation Testing 101 (Demo Using Katalon)
                                                
                                            
                                            
                                                27 Oct 2022 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            350.000

                                                                                                                    FREE
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    UI/UX Research &amp; Design (Hands On With Figma)
                                                    
                                                
                                                
                                                    With Sindy Larasati
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                UI/UX Research &amp; Design (Hands On With Figma)
                                                
                                            
                                            
                                                01 Oct 2022 |
                                                09:00 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    FREE
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Shifting Career To Data Science
                                                    
                                                
                                                
                                                    With Eric Julianto
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                Shifting Career To Data Science
                                                
                                            
                                            
                                                16 Sep 2022 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    FREE
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    How to Create an Effective &quot;Product Requirement Document&quot; (PRD)
                                                    
                                                
                                                
                                                    With Denny Yusuf
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                How to Create an Effective &quot;Product Requirement Document&quot; (PRD)
                                                
                                            
                                            
                                                05 Aug 2022 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            300.000

                                                                                                                    Rp
                                                                78.000
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                            
                
            
            

            
            
                Muat Lebih Banyak
                
            
        

        
            
                Loading...
            
        
    
</value>
      <webElementGuid>7f1db9a0-57f4-417d-9abf-600caa3cb3ee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;containerEvent&quot;)/div[1]</value>
      <webElementGuid>7d9043a8-499c-4a88-acb1-23877240e1fc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='containerEvent']/div</value>
      <webElementGuid>6f5a133d-70ac-4091-ac73-0562f75facb5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CODING.ID Event'])[1]/following::div[3]</value>
      <webElementGuid>5eee3500-4fde-46a6-bfb4-0794fa9332e3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div[2]/div</value>
      <webElementGuid>3320ee46-4ad8-4d04-ab7e-c96fcff25d04</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    
        
            Events
        
        

            
        
    


    
                    
                
                    
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 3: Predict using Machine Learning
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            OPEN until
                                                        18 Nov 2023 11:30 WIB
                                                
                                                                                        Mini Class
                                            
                                            
                                                Day 3: Predict using Machine Learning
                                                
                                            
                                            
                                                18 Nov 2023 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    Rp
                                                                85.000
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 4: Workshop
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            OPEN until
                                                        25 Nov 2023 11:30 WIB
                                                
                                                                                        Mini Class
                                            
                                            
                                                Day 4: Workshop
                                                
                                            
                                            
                                                25 Nov 2023 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    Rp
                                                                85.000
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 2: Data Wrangling with Python
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                Day 2: Data Wrangling with Python
                                                
                                            
                                            
                                                11 Nov 2022 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    Rp
                                                                85.000
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 1: Introduction to Python for Data Scientist
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                Day 1: Introduction to Python for Data Scientist
                                                
                                            
                                            
                                                04 Nov 2022 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    Rp
                                                                85.000
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Booster Class: Be Data Scientist and Machine Learning Engineer in 4 Days
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                Booster Class: Be Data Scientist and Machine Learning Engineer in 4 Days
                                                
                                            
                                            
                                                04 Nov 2022 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            2.000.000

                                                                                                                    Rp
                                                                200.000
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Automation Testing 101 (Demo Using Katalon)
                                                    
                                                
                                                
                                                    With Impola Tonatua Surya
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                Automation Testing 101 (Demo Using Katalon)
                                                
                                            
                                            
                                                27 Oct 2022 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            350.000

                                                                                                                    FREE
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    UI/UX Research &amp; Design (Hands On With Figma)
                                                    
                                                
                                                
                                                    With Sindy Larasati
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                UI/UX Research &amp; Design (Hands On With Figma)
                                                
                                            
                                            
                                                01 Oct 2022 |
                                                09:00 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    FREE
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Shifting Career To Data Science
                                                    
                                                
                                                
                                                    With Eric Julianto
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                Shifting Career To Data Science
                                                
                                            
                                            
                                                16 Sep 2022 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    FREE
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    How to Create an Effective &quot;Product Requirement Document&quot; (PRD)
                                                    
                                                
                                                
                                                    With Denny Yusuf
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                How to Create an Effective &quot;Product Requirement Document&quot; (PRD)
                                                
                                            
                                            
                                                05 Aug 2022 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            300.000

                                                                                                                    Rp
                                                                78.000
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                            
                
            
            

            
            
                Muat Lebih Banyak
                
            
        

        
            
                Loading...
            
        
    
' or . = '
    
        
            Events
        
        

            
        
    


    
                    
                
                    
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 3: Predict using Machine Learning
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            OPEN until
                                                        18 Nov 2023 11:30 WIB
                                                
                                                                                        Mini Class
                                            
                                            
                                                Day 3: Predict using Machine Learning
                                                
                                            
                                            
                                                18 Nov 2023 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    Rp
                                                                85.000
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 4: Workshop
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            OPEN until
                                                        25 Nov 2023 11:30 WIB
                                                
                                                                                        Mini Class
                                            
                                            
                                                Day 4: Workshop
                                                
                                            
                                            
                                                25 Nov 2023 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    Rp
                                                                85.000
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 2: Data Wrangling with Python
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                Day 2: Data Wrangling with Python
                                                
                                            
                                            
                                                11 Nov 2022 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    Rp
                                                                85.000
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 1: Introduction to Python for Data Scientist
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                Day 1: Introduction to Python for Data Scientist
                                                
                                            
                                            
                                                04 Nov 2022 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    Rp
                                                                85.000
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Booster Class: Be Data Scientist and Machine Learning Engineer in 4 Days
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                Booster Class: Be Data Scientist and Machine Learning Engineer in 4 Days
                                                
                                            
                                            
                                                04 Nov 2022 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            2.000.000

                                                                                                                    Rp
                                                                200.000
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Automation Testing 101 (Demo Using Katalon)
                                                    
                                                
                                                
                                                    With Impola Tonatua Surya
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                Automation Testing 101 (Demo Using Katalon)
                                                
                                            
                                            
                                                27 Oct 2022 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            350.000

                                                                                                                    FREE
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    UI/UX Research &amp; Design (Hands On With Figma)
                                                    
                                                
                                                
                                                    With Sindy Larasati
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                UI/UX Research &amp; Design (Hands On With Figma)
                                                
                                            
                                            
                                                01 Oct 2022 |
                                                09:00 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    FREE
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Shifting Career To Data Science
                                                    
                                                
                                                
                                                    With Eric Julianto
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                Shifting Career To Data Science
                                                
                                            
                                            
                                                16 Sep 2022 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    FREE
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    How to Create an Effective &quot;Product Requirement Document&quot; (PRD)
                                                    
                                                
                                                
                                                    With Denny Yusuf
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                How to Create an Effective &quot;Product Requirement Document&quot; (PRD)
                                                
                                            
                                            
                                                05 Aug 2022 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            300.000

                                                                                                                    Rp
                                                                78.000
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                            
                
            
            

            
            
                Muat Lebih Banyak
                
            
        

        
            
                Loading...
            
        
    
')]</value>
      <webElementGuid>686f2e6b-41dc-41cf-a0f4-8cf1d863d817</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
