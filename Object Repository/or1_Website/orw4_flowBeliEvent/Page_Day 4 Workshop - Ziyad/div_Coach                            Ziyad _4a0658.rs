<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Coach                            Ziyad _4a0658</name>
   <tag></tag>
   <elementGuidId>cde4d1ba-3932-4059-bffa-98f27a4528e5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Kelas akan diadakan dalam bahasa Indonesia'])[1]/following::div[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.wm-event-options</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>f361d1e5-29e9-4ce4-b285-962d7d516e13</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>wm-event-options</value>
      <webElementGuid>25ebae3f-3dc7-4795-ad94-6b2f05d23808</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                        
                                                            
                                                    

                        
                            Coach
                            Ziyad Syauqi Fawwazi
                            
                            
                                Data Scientist Market Leader Company in Automotive Industry
                            
                        
                    
                </value>
      <webElementGuid>4c49f964-4a0f-4496-ad80-0e032b792596</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;no-htmlimports no-flash no-proximity no-applicationcache blobconstructor blob-constructor cookies cors customprotocolhandler dataview eventlistener geolocation history no-ie8compat json notification queryselector serviceworker customevent postmessage svg templatestrings typedarrays websockets no-xdomainrequest webaudio webworkers no-contextmenu cssall audio canvas canvastext contenteditable emoji olreversed no-userdata video no-vml webanimations webgl adownload audioloop canvasblending todataurljpeg todataurlpng todataurlwebp canvaswinding no-ambientlight hashchange inputsearchevent pointerevents no-hiddenscroll mathml unicoderange no-touchevents no-unicode no-batteryapi no-battery-api crypto no-dart gamepads fullscreen indexeddb indexeddb-deletedatabase intl pagevisibility performance pointerlock quotamanagement requestanimationframe raf vibrate no-webintents no-lowbattery getrandomvalues backgroundblendmode cssanimations backdropfilter backgroundcliptext appearance exiforientation audiopreload&quot;]/body[@class=&quot;wm-sticky&quot;]/div[@class=&quot;wm-main-wrapper&quot;]/div[@class=&quot;wm-main-section&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/aside[@class=&quot;col-md-4&quot;]/div[@class=&quot;wm-event-options&quot;]</value>
      <webElementGuid>59a76ce1-b9be-46bd-abab-2c59de3f7a83</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kelas akan diadakan dalam bahasa Indonesia'])[1]/following::div[1]</value>
      <webElementGuid>1b5959a6-9a68-44c5-b0c9-2573a08b95be</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Link Zoom akan dikirimkan via WA H-1 sampai maksimal 2 jam sebelum kelas dimulai'])[1]/following::div[1]</value>
      <webElementGuid>182444e5-7480-4180-b90a-83a67a885f6d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//aside/div</value>
      <webElementGuid>e7ce2cea-f4b3-4f96-88d5-6f245714f8c9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                    
                        
                                                            
                                                    

                        
                            Coach
                            Ziyad Syauqi Fawwazi
                            
                            
                                Data Scientist Market Leader Company in Automotive Industry
                            
                        
                    
                ' or . = '
                    
                        
                                                            
                                                    

                        
                            Coach
                            Ziyad Syauqi Fawwazi
                            
                            
                                Data Scientist Market Leader Company in Automotive Industry
                            
                        
                    
                ')]</value>
      <webElementGuid>f7974f4f-7278-4901-a42b-36ccac2b6a83</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
