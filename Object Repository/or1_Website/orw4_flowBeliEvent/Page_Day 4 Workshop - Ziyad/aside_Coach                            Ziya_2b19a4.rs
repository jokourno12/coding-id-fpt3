<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>aside_Coach                            Ziya_2b19a4</name>
   <tag></tag>
   <elementGuidId>34435803-b323-43ca-bae1-5a90eef9fdef</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Kelas akan diadakan dalam bahasa Indonesia'])[1]/following::aside[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>aside.col-md-4</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>aside</value>
      <webElementGuid>042fbf9a-ae4c-4edd-a1b3-4abd7865955f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-md-4</value>
      <webElementGuid>6ecd29e9-1644-4a4f-9de4-6e6ad0359f22</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>

                
                    
                        
                                                            
                                                    

                        
                            Coach
                            Ziyad Syauqi Fawwazi
                            
                            
                                Data Scientist Market Leader Company in Automotive Industry
                            
                        
                    
                
                
                    
                        
                        
                            Harga Kelas
                                                                                                Rp.
                                        500.000
                                                                                                                        Rp
                                    85.000
                            
                        

                        
                            
                            Tanggal:
                            25 November 2023
                            
                            
                        
                        
                            
                            Jam:
                            
                            19:30 
                                WIB 
                        


                        
                            
                            Lokasi


                            Zoom

                        

                                                
                            
                            Waktu Pendaftaran Tersisa
                            
                            79 Hari 18 Jam 15  Menit  55 Detik 
                            

                        
                        
                                                                                                
                                                                                                                        Beli Tiket
                                        
                                                                    

                                                        

                            
                        


                    
                

            </value>
      <webElementGuid>5c2aab43-0b77-46e6-83a2-aeee57aa382b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;no-htmlimports no-flash no-proximity no-applicationcache blobconstructor blob-constructor cookies cors customprotocolhandler dataview eventlistener geolocation history no-ie8compat json notification queryselector serviceworker customevent postmessage svg templatestrings typedarrays websockets no-xdomainrequest webaudio webworkers no-contextmenu cssall audio canvas canvastext contenteditable emoji olreversed no-userdata video no-vml webanimations webgl adownload audioloop canvasblending todataurljpeg todataurlpng todataurlwebp canvaswinding no-ambientlight hashchange inputsearchevent pointerevents no-hiddenscroll mathml unicoderange no-touchevents no-unicode no-batteryapi no-battery-api crypto no-dart gamepads fullscreen indexeddb indexeddb-deletedatabase intl pagevisibility performance pointerlock quotamanagement requestanimationframe raf vibrate no-webintents no-lowbattery getrandomvalues backgroundblendmode cssanimations backdropfilter backgroundcliptext appearance exiforientation audiopreload&quot;]/body[@class=&quot;wm-sticky&quot;]/div[@class=&quot;wm-main-wrapper&quot;]/div[@class=&quot;wm-main-section&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/aside[@class=&quot;col-md-4&quot;]</value>
      <webElementGuid>d8352b61-1114-406b-8e1d-4e950266bc41</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kelas akan diadakan dalam bahasa Indonesia'])[1]/following::aside[1]</value>
      <webElementGuid>412ba083-3514-4a9b-9078-1f54515929a5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Link Zoom akan dikirimkan via WA H-1 sampai maksimal 2 jam sebelum kelas dimulai'])[1]/following::aside[1]</value>
      <webElementGuid>d7a037b0-bab2-48ba-ad90-ecda40505f01</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//aside</value>
      <webElementGuid>7218039c-80ae-476a-820e-7bef8b457966</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//aside[(text() = '

                
                    
                        
                                                            
                                                    

                        
                            Coach
                            Ziyad Syauqi Fawwazi
                            
                            
                                Data Scientist Market Leader Company in Automotive Industry
                            
                        
                    
                
                
                    
                        
                        
                            Harga Kelas
                                                                                                Rp.
                                        500.000
                                                                                                                        Rp
                                    85.000
                            
                        

                        
                            
                            Tanggal:
                            25 November 2023
                            
                            
                        
                        
                            
                            Jam:
                            
                            19:30 
                                WIB 
                        


                        
                            
                            Lokasi


                            Zoom

                        

                                                
                            
                            Waktu Pendaftaran Tersisa
                            
                            79 Hari 18 Jam 15  Menit  55 Detik 
                            

                        
                        
                                                                                                
                                                                                                                        Beli Tiket
                                        
                                                                    

                                                        

                            
                        


                    
                

            ' or . = '

                
                    
                        
                                                            
                                                    

                        
                            Coach
                            Ziyad Syauqi Fawwazi
                            
                            
                                Data Scientist Market Leader Company in Automotive Industry
                            
                        
                    
                
                
                    
                        
                        
                            Harga Kelas
                                                                                                Rp.
                                        500.000
                                                                                                                        Rp
                                    85.000
                            
                        

                        
                            
                            Tanggal:
                            25 November 2023
                            
                            
                        
                        
                            
                            Jam:
                            
                            19:30 
                                WIB 
                        


                        
                            
                            Lokasi


                            Zoom

                        

                                                
                            
                            Waktu Pendaftaran Tersisa
                            
                            79 Hari 18 Jam 15  Menit  55 Detik 
                            

                        
                        
                                                                                                
                                                                                                                        Beli Tiket
                                        
                                                                    

                                                        

                            
                        


                    
                

            ')]</value>
      <webElementGuid>b30c3b8e-3d31-4b73-b395-e96f66419072</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
