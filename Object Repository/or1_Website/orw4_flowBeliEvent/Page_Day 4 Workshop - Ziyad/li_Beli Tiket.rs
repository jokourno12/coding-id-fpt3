<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_Beli Tiket</name>
   <tag></tag>
   <elementGuidId>9024bed4-59b8-421c-951a-448ceac23aa2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='list-button']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#list-button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>9dd4781e-b244-466c-a304-4fe5760b0b63</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>list-button</value>
      <webElementGuid>378b4cfb-d9ea-4f29-8433-0c16cb09677c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                                                                
                                                                                                                        Beli Tiket
                                        
                                                                    

                                                        

                            
                        </value>
      <webElementGuid>e5132fea-f468-401c-84d0-78c831eb9a50</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;list-button&quot;)</value>
      <webElementGuid>ef4d4dc7-b666-4586-8f0e-9cd7a6481b10</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//li[@id='list-button']</value>
      <webElementGuid>6ad867a6-cff9-4b00-9232-ab4b9215e500</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Detik'])[1]/following::li[1]</value>
      <webElementGuid>3d95cb8f-abab-4928-8068-d562913609d4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Menit'])[1]/following::li[1]</value>
      <webElementGuid>36c484bc-ad05-4aa4-96e5-0350b35e46ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Event lain yang banyak diminati'])[1]/preceding::li[1]</value>
      <webElementGuid>6afa0958-ba3a-4af1-a5ba-0eb7012d07b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//aside/div[2]/ul/li[6]</value>
      <webElementGuid>e08b0564-4df5-4493-8576-1236cad31dae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[@id = 'list-button' and (text() = '
                                                                                                
                                                                                                                        Beli Tiket
                                        
                                                                    

                                                        

                            
                        ' or . = '
                                                                                                
                                                                                                                        Beli Tiket
                                        
                                                                    

                                                        

                            
                        ')]</value>
      <webElementGuid>98841616-58e4-4cdb-af63-6b490c7f9795</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
