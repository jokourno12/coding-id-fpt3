<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>img</name>
   <tag></tag>
   <elementGuidId>d11ca01b-f7e9-442f-8366-6a6dfb7bfd6d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#WA-Icon > img</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='WA-Icon']/img</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>2e77c98d-a4ba-4f58-9d67-6c6d0a8f1a3e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>https://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/WhatsApp.svg/1022px-WhatsApp.svg.png</value>
      <webElementGuid>5a858218-d1f6-470f-b010-dd3b919b6b64</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;WA-Icon&quot;)/img[1]</value>
      <webElementGuid>d12280c3-f387-4bc8-bcca-c41119ce24f4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='WA-Icon']/img</value>
      <webElementGuid>a78b84fb-3451-4354-afb6-c282ab7033f3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:img</name>
      <type>Main</type>
      <value>//img[contains(@src,'https://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/WhatsApp.svg/1022px-WhatsApp.svg.png')]</value>
      <webElementGuid>dd60f507-b5d5-4723-9c01-933d5fa22d59</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/a/img</value>
      <webElementGuid>41b3c076-a1e3-4aa6-8f02-0859fbb80853</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//img[@src = 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/WhatsApp.svg/1022px-WhatsApp.svg.png']</value>
      <webElementGuid>c9b58bc0-6560-4e0d-bd02-2b8d7196ca35</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
