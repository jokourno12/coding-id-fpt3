<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_62 812-7319-369</name>
   <tag></tag>
   <elementGuidId>360175a7-8746-4b7b-891f-4c818c7ee399</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>p > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main_block']/div/h2/p/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>8898363c-7cd9-4b4e-a6a0-4793f7b3864e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>+62 812-7319-369</value>
      <webElementGuid>ad68731a-a099-49ce-904b-8f3ab69170af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main_block&quot;)/div[@class=&quot;_9vd6 _9t33 _9bir _9bj3 _9bhj _9v12 _9tau _9tay _9u6w _9se- _9u5y&quot;]/h2[@class=&quot;_9vd5 _9scb&quot;]/p[1]/span[1]</value>
      <webElementGuid>d17a38cd-99e7-4093-b9ce-f593b4fb36db</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main_block']/div/h2/p/span</value>
      <webElementGuid>37c34617-b562-4d41-a68f-0d00994ca41c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Download'])[2]/following::span[2]</value>
      <webElementGuid>eeffbcf2-00c0-4e2c-8801-8b88088d8014</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='WhatsApp Web'])[1]/following::span[3]</value>
      <webElementGuid>d626dcd4-0815-49f1-82c5-de7095cd8e32</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Continue to Chat'])[1]/preceding::span[1]</value>
      <webElementGuid>ea169699-a996-417e-a5ec-746b351d3515</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('Don', &quot;'&quot;, 't have WhatsApp yet?')])[1]/preceding::span[2]</value>
      <webElementGuid>97292e91-de4a-424f-9f4a-a3d999462bb0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='+62 812-7319-369']/parent::*</value>
      <webElementGuid>9b94691a-a342-414e-9875-86ffc00758c8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//p/span</value>
      <webElementGuid>52e042f8-b1c2-447f-9633-41ceedb9c6d3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '+62 812-7319-369' or . = '+62 812-7319-369')]</value>
      <webElementGuid>37ed2d93-4905-4355-969e-e4b546ef11ec</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
